const fs = require('fs');
const path = require('path')
const { exit } = require('process')
const xml2js = require('xml2js');
const types = {'str': 4, 'float': 2, 'int': 1};
const serializer = require('./src/serializer');
 
const inputDirectory = path.join(__dirname, 'xml/');

if(!fs.existsSync(inputDirectory)) {
    console.log("xml directory does not exist! Exiting...")
    exit(0)
}

for (file of fs.readdirSync(inputDirectory)) {
    const xml = fs.readFileSync(`${inputDirectory}/${file}`);
    const parser = new xml2js.Parser();

    parser.parseString(xml, function (err, parsed) {
        if (err) {
            console.error(err);
            return;
        }

        if (!'BinaryPrototype' in parsed) {
            console.error('Unknown XML root - is this a prototype?')
            return;
        }

        const root = parsed.BinaryPrototype;
        const prototype = unpackElement(root.Prototype[0], 'Prototype');
        const e4p = {path: root.$.path, root: prototype};
        // console.dir(e4p, {depth: 7});

        const bin = serializer.serialize(e4p);
        // console.log(bin);

        let filename = path.parse(file).name.concat(".e4p")
        fs.mkdir('out', (err) => {
            if (err && err.code !== 'EEXIST') throw err;
        })
        fs.writeFileSync(`out/${filename}`, bin);
    });
}

function unpackElement(element, name) {
    const unpacked = {attributes: [], children: [], name: {value: name, length: name.length}};

    for (const property of Object.keys(element)) {
        if (property === '$') {
            unpacked.attributes = unpackAttributes(element[property]);
        } else {
            for (const subnode of element[property]) {
                const child = unpackElement(subnode, property);
                unpacked.children.push(child);
            }
        }
    }

    unpacked.attributeCount = unpacked.attributes.length;
    unpacked.childrenCount = unpacked.children.length;

    return unpacked;
}

function unpackAttributes(attributes) {
    const unpacked = [];

    for (const attribute of Object.keys(attributes)) {
        const [type, key] = attribute.split(':');
        const value = asType(type, attributes[attribute]);
        unpacked.push({key, type: types[type], value});
    }

    return unpacked;
}

function asType(type, value) {
    switch (type) {
        case 'float':
            return parseFloat(value);
        case 'int':
            return parseInt(value);
        default:
            return value;
    }
}