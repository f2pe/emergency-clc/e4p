const fs = require('fs');
const path = require('path')
const { exit } = require('process')
const builder = require('xmlbuilder');
const parser = require('./src/parser');
const map = {};

const types = {1: 'int', 2: 'float', 4:'str'};

const inputDirectory = path.join(__dirname, 'in/');

if(!fs.existsSync(inputDirectory)) {
    console.log("Input directory does not exist! Exiting...")
    exit(0)
}

let ptr;
for (file of fs.readdirSync(inputDirectory)) {
    const prototype = parser.parse(fs.readFileSync(`${inputDirectory}/${file}`));
    ptr = file;
    map[ptr] = [];
    const xml = toXML(prototype);
    fs.mkdir('xml', (err) => {
        if (err && err.code !== 'EEXIST') throw err;
    })
    fs.writeFileSync(`xml/${file.replace('.e4p', '.xml')}`, xml);
}

function writeElement(parent, element) {
    if (element.name.value === 'Child') {
        const at = element.attributes.find(x => x.key === 'ID');
        map[ptr].push(+at.value);
    }

    const x = parent.ele(element.name.value);
    for (const attribute of element.attributes) {
        x.att(types[attribute.type] + ':' +  attribute.key, attribute.value);
    }
    for (const child of element.children) {
        writeElement(x, child);
    }
    return x;
}

function toXML(prototype) {
    const root = builder.create('BinaryPrototype');
    root.att('path', prototype.path);
    root.att('generator', '1.1.1');
    return writeElement(root, prototype.root).end({pretty: true});    
}
