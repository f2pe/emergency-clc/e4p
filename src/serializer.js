/**
 * Convert object to .e4p buffer
 * @param {object} prototype Prototype object from JSON
 * @returns {Buffer} Serialized prototype
 */
function serialize(prototype) {
    const buffer = Buffer.alloc(128000);

    let offset = writeEm4String(prototype.path, buffer);
    offset = buffer.writeUInt32LE(0x01, offset);
    offset = writeElement(prototype.root, buffer, offset);

    if (offset === 128000) {
        throw new Error('Prototype too large');
    }

    return buffer.slice(0, offset);
}

function em4String(str) {
    const buffer = Buffer.alloc(4 + str.length * 2);
    writeEm4String(str, buffer);
    return buffer;
}

/**
 * Write an "Em4 String" to the buffer
 * @param {string} str 
 * @param {Buffer} buffer 
 * @param {number} offset 
 */
function writeEm4String(str, buffer, offset = 0) {
    offset = buffer.writeInt32LE(str.length, offset);
    // write returns only (!) number of written bytes
    offset += buffer.write(str, offset, 'utf16le');
    return offset;
}

function attribute(attribute) {
    // calculate attribute size
    const buffer = Buffer.alloc(
        4 + attribute.key.length * 2 + // key
        4 + // type
        (attribute.type == 4 ? // value
            4 + attribute.value.length * 2 :
            4)
    );

    writeAttribute(attribute, buffer, 0);

    return buffer;
}

/**
 * Write an attribute to the buffer
 * @param {object} attribute 
 * @param {Buffer} buffer 
 * @param {Number} offset 
 */
function writeAttribute(attribute, buffer, offset = 0) {
    // write key
    offset = writeEm4String(attribute.key, buffer, offset);
    // write type
    offset = buffer.writeUInt32LE(attribute.type, offset);
    // write value
    switch (attribute.type) {
        case 1:
            offset = buffer.writeInt32LE(+attribute.value, offset);
            break;
        case 2:
            offset = buffer.writeFloatLE(+attribute.value, offset);
            break;
        case 4:
            offset = writeEm4String(attribute.value, buffer, offset);
            break;
        default:
            throw new Error(`Invalid attribute type ${attribute.type}`);
    }
    return offset;
}

function writeElement(element, buffer, offset = 0) {
    offset = buffer.writeUInt32LE(0x01, offset); // magic
    offset = writeEm4String(element.name.value, buffer, offset); // name

    // write children
    offset = buffer.writeUInt32LE(element.children.length, offset);
    for (const child of element.children) {
        offset = writeElement(child, buffer, offset);
    }

    // write attributes
    offset = buffer.writeUInt32LE(element.attributes.length, offset);
    for (const attribute of element.attributes) {
        offset = writeAttribute(attribute, buffer, offset);
    }

    return offset;
}


module.exports = {
    serialize, em4String, attribute
};