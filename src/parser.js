const Parser = require('binary-parser').Parser;

const valueExtractFormatter = (parserResult) => {
    if (Object.is(parserResult.value, -0)) {
        return '-0';
    }
    return parserResult.value;
};

/**
 * Parser for "em4 strings" in e4p files
 * 
 * Serialized as length + value, where length is 
 * the length of utf-16 symbols in value
 */
const em4String = new Parser()
    .endianess('little')
    .uint32('length')
    .string('value', {
        length: function () {
            return this.length * 2;
        },
        encoding: 'utf16le'
    });

const integer = new Parser()
    .int32le('value');

const float = new Parser()
    .floatle('value');

/**
 * Parse attributes with exact value type
 */
const attribute = new Parser()
    .endianess('little')
    .nest('key', {
        type: em4String,
        formatter: valueExtractFormatter
    })
    .uint32('type')
    .choice('value', {
        tag: 'type',
        choices: {
            1: integer,
            2: float,
            4: em4String
        },
        formatter: valueExtractFormatter
    });

/**
 * Recursive structure
 * 01 00 00 00 Element
 * 08 00 00 00 43 00 68 00 69 00 6C 00 64 00 72 00 65 00 6E 00 em4String i.e. "Children"
 *   00 00 00 00 childrenCount
 *   ... child elements (recursive)
 *   00 00 00 00 attributeCount
 *   ... attributes                    
 */
const element = new Parser()
    .namely('element')
    .endianess('little')
    .uint32('key', { assert: 0x01 })
    .nest('name', {
        type: em4String,
        value: valueExtractFormatter
    })
    .uint32('childrenCount')
    .array('children', {
        type: 'element', // forward reference (element not yet defined)
        length: 'childrenCount'
    })
    .uint32('attributeCount')
    .array('attributes', {
        type: attribute,
        length: 'attributeCount'
    });

/**
 * "Root" parser
 * Can read e4p "header" (file path and magic number 0x01) and recursively parse all children
 */
const parser = new Parser()
    .endianess('little')
    .nest('path', {
        type: em4String,
        formatter: valueExtractFormatter
    })
    .uint32('magic', { assert: 0x01 })
    .nest('root', {
        type: element
    });


/**
 * Parse an uncompressed .e4p prototype
 * @param {Buffer} buffer Buffer of file
 */
function parse(buffer) {
    return parser.parse(buffer);
}

module.exports = { parse };