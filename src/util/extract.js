const fs = require('fs');
const path = require('path');
const merge = require('deepmerge');
const expectFile = path.join(__dirname, '../test/expect.json');

// Fetch all prototypes from expect.json
const prototypes = Object.values(JSON.parse(fs.readFileSync(expectFile)));


const emptyTarget = value => Array.isArray(value) ? [] : {}
const clone = (value, options) => merge(emptyTarget(value), value, options)

const combineMerge = (target, source, options) => {
	const destination = target.slice()

	source.forEach((item, index) => {
		if (typeof destination[index] === 'undefined') {
			const cloneRequested = options.clone !== false
			const shouldClone = cloneRequested && options.isMergeableObject(item)
			destination[index] = shouldClone ? clone(item, options) : item
		} else if (options.isMergeableObject(item)) {
			destination[index] = merge(target[index], item, options)
		} else if (target.indexOf(item) === -1) {
			destination.push(item)
		}
	})
	return destination
};

// function addProperty(collect, object, property) {
//     if (property in collect) {
//         collect[]
//     }
// }
console.log(`${prototypes.length} prototypes loaded`);
const combined = merge.all(prototypes, { arrayMerge: combineMerge });
console.log(combined);