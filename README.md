# e4p - Emergency 4 prototype reader and writer
## Installation
- `yarn install @emergencyx/e4p`

## Usage
Parse a prototype
```
    const fs = require('fs');
    const e4p = require('@emergencyx/e4p'); // Import e4p

    buffer = fs.readFileSync('file.e4p');
    prototype = e4p.parse(buffer); // Returns decoded prototype
```
    
Write a prototype
```
    const fs = require('fs');
    const e4p = require('@emergencyx/e4p'); // Import e4p
    ...
    buffer = e4p.serialize(prototype); // Convert prototype to .e4p buffer
```

## Testing
- `yarn test`

Parses and serializes .e4p files placed in `test/input/`.

Test data must be compressed before committing: `tar -zcf input.tar.gz test/input`.
Take care not to accidentally include any other files, e.g. test/test.js.